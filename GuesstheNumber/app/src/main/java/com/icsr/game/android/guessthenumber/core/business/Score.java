package com.icsr.game.android.guessthenumber.core.business;

import android.content.Context;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.model.Stage;
import com.icsr.game.android.guessthenumber.support.Utils;

import java.util.Map;

public class Score {

    public static int getScores(Context context){
        Map<Integer, Stage> stages = Utils.loadStages(context);

        int score = 0;
        for(Stage s : stages.values()){
            score += getScore(context, s);
        }

        return score;
    }

    public static int getScore(Context context, Stage stage) {
        if(stage.getHits() == 0 || stage.getTime() == 0){
            return 0;
        }
        long score = Utils.getInt(context, R.integer.scoreBonusStage);
        score += Utils.getInt(context, R.integer.scoreBonusStage)*(stage.getMaxTime() - stage.getTime())/10000L;
        score -= Utils.getInt(context, R.integer.scoreBonusStage)/10*stage.getHits();

        return Math.round(score);
    }

}
