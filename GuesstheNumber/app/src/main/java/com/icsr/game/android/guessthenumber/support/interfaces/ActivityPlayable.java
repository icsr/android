package com.icsr.game.android.guessthenumber.support.interfaces;

import android.app.Activity;
import android.content.Context;
import android.content.SharedPreferences;

public interface ActivityPlayable {

    void init();

    int getLayoutRid();

    void closeKeyboard();

    void callNextActivity(Class<? extends Activity> activity, int delay);

    void callNextActivity(Class<? extends Activity> activity);

    Context getContext();

    String getString(int tipTextButton);

    int getInt(int R_ID_KEY);

    float getFloat(int R_ID_KEY);

    SharedPreferences getPreferences();

    void rumble(int R_ID);

    void click();

    void play(int R_ID);

    int getCurrentAudioPosition(int R_ID);

    void setCurrentAudioPosition(int clickSoundKey, int i);
}
