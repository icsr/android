package com.icsr.game.android.guessthenumber.support;

import android.content.Context;

import androidx.annotation.NonNull;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;

public class FileUtils<T> {

    private Context context;

    public FileUtils(Context context) {
        this.context = context;
    }

    public boolean save(@NonNull int R_ID_KEY, @NonNull Object obj) {
        try {
            FileOutputStream fos = context.openFileOutput(context.getString(R_ID_KEY), Context.MODE_PRIVATE);
            ObjectOutputStream out = new ObjectOutputStream(fos);
            out.writeObject(obj);
            out.flush();
            out.close();
            fos.close();

            return true;
        } catch (IOException e) {
            e.printStackTrace();
            return false;
        }
    }

    private File getFile(int R_ID_KEY) {
        return new File(context.getFilesDir(), context.getString(R_ID_KEY));
    }

    public boolean exists(int R_ID_KEY) {
        return getFile(R_ID_KEY).exists();
    }

    public T load(int R_ID_KEY) throws IOException, ClassNotFoundException {
        if (!exists(R_ID_KEY)) {
            throw new FileNotFoundException();
        }

        FileInputStream fin = context.openFileInput(context.getString(R_ID_KEY));
        ObjectInputStream in = new ObjectInputStream(fin);
        T input = (T) in.readObject();
        in.close();
        fin.close();

        return input;

    }

}
