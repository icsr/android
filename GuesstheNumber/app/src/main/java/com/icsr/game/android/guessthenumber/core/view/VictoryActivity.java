package com.icsr.game.android.guessthenumber.core.view;

import android.content.Intent;
import android.os.Handler;
import android.view.View;
import android.widget.TextView;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.business.Score;
import com.icsr.game.android.guessthenumber.support.Command;
import com.icsr.game.android.guessthenumber.support.Utils;

public class VictoryActivity extends InterActivity {
    private TextView victoryTitle;
    private TextView victoryCodes;
    private View okButton;

    @Override
    public void init() {
        initRumble();
        initView();
        initEvents();
        initSound();
    }

    @Override
    public int getLayoutRid() {
        return R.layout.activity_victory;
    }

    public void initSound() {
        audio.play(R.raw.victory);
    }

    private void initRumble() {
        rumble(getInt(R.integer.victoryRumbleDuration));
    }

    private void initEvents() {
        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click();

                new Handler().postDelayed(new Runnable() {
                    @Override
                    public void run() {
                        startActivity(new Intent(getApplicationContext(), ScoresActivity.class));
                        finish();
                    }
                }, 200);
            }
        });
    }

    private void initView() {
        victoryTitle = findViewById(R.id.victoryTitle);
        victoryCodes = findViewById(R.id.victoryCodes);
        victoryTitle.setText(String.format("%s\n\nYou made a %s pts.",
                getString(R.string.victory), Utils.formatNumber(Score.getScores(this))));

        StringBuilder str = new StringBuilder("I think you really should try these codes:\n");

        for (String s : Command.COMMAND.listCommons()) {
            str.append(s).append(", ");
        }

        str.replace(str.length() - 2, str.length(), "");
        str.append("\nNo matter what, never try code " + Command.COMMAND.RESET_ALL_GAMES.getCode());
        victoryCodes.setText(str.toString());

        okButton = findViewById(R.id.okVictoryButton);
    }
}
