package com.icsr.game.android.guessthenumber.support;

import android.content.DialogInterface;

import androidx.appcompat.app.AlertDialog;

import com.google.android.material.dialog.MaterialAlertDialogBuilder;
import com.icsr.game.android.guessthenumber.core.view.InterActivity;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;

public class Alert extends MaterialAlertDialogBuilder {
    private Map<String, MaterialAlertDialogBuilder> instances = new HashMap<>();
    private LinkedList<String> keys = new LinkedList<>();
    private AlertDialog current;

    public Alert(InterActivity activityPlayable) {
        super(activityPlayable);
    }

    public void show(String title, String message){
        this.setTitle(title);
        this.setMessage(message);

        if(isOnQueue(title, message)){
            return;
        }

        grabFirstOrNextAlert(add(title, message));
    }

    private MaterialAlertDialogBuilder add(String title, String message) {
        MaterialAlertDialogBuilder m = new MaterialAlertDialogBuilder(getContext());
        m.setTitle(title);
        m.setMessage(message);
        m.setPositiveButton("Ok", null);

        String key = title+message;
        keys.add(key);
        instances.put(key, m);
        return m;
    }

    private void grabFirstOrNextAlert(MaterialAlertDialogBuilder m) {
        grabNextAlertAfterPreviousAlertHasBeenDismissed(m);
        grabFirstAlert();
    }

    private boolean isOnQueue(String title, String message) {
        return instances.keySet().contains(title+message);
    }

    private void grabNextAlertAfterPreviousAlertHasBeenDismissed(MaterialAlertDialogBuilder m) {
        m.setOnDismissListener(new DialogInterface.OnDismissListener() {
            @Override
            public void onDismiss(DialogInterface dialog) {
                if(instances.isEmpty()){
                    current = null;
                    return;
                }
                current = instances.remove(keys.pop()).show();

            }
        });
    }

    private void grabFirstAlert() {
        if(current == null) {
            current = instances.remove(keys.pop()).show();
        }
    }

}
