package com.icsr.game.android.guessthenumber.core.business;

import android.media.MediaPlayer;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.model.State;
import com.icsr.game.android.guessthenumber.support.interfaces.ActivityPlayable;

import java.util.HashMap;
import java.util.Map;

public class AudioHolder {

    private final ActivityPlayable activity;
    private static Map<Integer, MediaPlayer> audioPool;

    public AudioHolder(ActivityPlayable activity) {
        audioPool = new HashMap<>();
        this.activity = activity;
        init();
    }

    private MediaPlayer getSound(int R_RAW_ID) {
        return audioPool.get(R_RAW_ID);
    }

    public MediaPlayer putSound(int R_RAW_ID) {
        audioPool.put(R_RAW_ID, MediaPlayer.create(activity.getContext(), R_RAW_ID));
        return getSound(R_RAW_ID);
    }

    void init() {

        if(audioPool == null) {
            audioPool = new HashMap<>();
        }else{
            audioPool.clear();
        }

        MediaPlayer back = putSound(R.raw.back);
        back.setLooping(true);

        State state = State.newSingleton(activity);

        if (state.hasKey(R.string.music)) {
            back.seekTo(state.getInt(R.string.music));
        } else {
            back.seekTo(1000);
        }

        putSound(R.raw.youwin);
        putSound(R.raw.click);
        putSound(R.raw.siren);
        putSound(R.raw.back);
        putSound(R.raw.back0);
        putSound(R.raw.victory);

        MediaPlayer trySound = putSound(R.raw.tryagain);
        float ratio = activity.getFloat(R.string.volume_ratio);
        trySound.setVolume(ratio, ratio);
    }

    public void play(int R_RAW_ID) {
        play(R_RAW_ID, 0);
    }

    public void play(int R_RAW_ID, int position) {
        try {
            MediaPlayer sound = getSound(R_RAW_ID);
            sound.seekTo(position);
            sound.start();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public int getCurrentPosition(int R_ID) {
        return getSound(R_ID).getCurrentPosition();
    }

    public void setCurrentPosition(int R_ID, int position) {
        getSound(R_ID).seekTo(position);
    }

    public void pauseAll() {
        for (MediaPlayer m : audioPool.values()) {
            if(m.isPlaying()) {
                m.pause();
            }
        }
    }

    public void click() {
        play(R.raw.click);
    }
}
