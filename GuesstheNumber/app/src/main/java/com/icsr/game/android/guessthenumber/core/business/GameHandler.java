package com.icsr.game.android.guessthenumber.core.business;

import android.graphics.Color;
import android.os.Bundle;
import android.os.Handler;
import android.view.KeyEvent;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.icsr.game.android.guessthenumber.support.Alert;
import com.icsr.game.android.guessthenumber.support.Command;
import com.icsr.game.android.guessthenumber.core.view.InterActivity;
import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.view.VictoryActivity;
import com.icsr.game.android.guessthenumber.core.model.Stage;
import com.icsr.game.android.guessthenumber.core.model.State;
import com.icsr.game.android.guessthenumber.support.Timer;
import com.icsr.game.android.guessthenumber.support.Tips;
import com.icsr.game.android.guessthenumber.support.Utils;
import com.icsr.game.android.guessthenumber.support.event.numberTypedWatcher;
import com.icsr.game.android.guessthenumber.support.interfaces.ActivityPlayable;
import com.icsr.game.android.guessthenumber.support.interfaces.Playable;

import java.util.Map;

public class GameHandler implements Playable {
    private final ActivityPlayable activity;
    private final ViewHolder viewHandler;
    private Tips tips = new Tips();
    private int hiddenNumber;
    private int stageIndex = 0;
    private int countGuessing = 0;
    private Map<Integer, Stage> stages;
    private Timer timer;
    private Alert alert;
    private int tipsLeft;

    public GameHandler(InterActivity activity, ViewHolder viewHandler) {
        this.activity = activity;
        this.viewHandler = viewHandler;
        timer = new Timer(this);
        alert = new Alert(activity);

        initViewEvents();
        initGame();
    }

    private void setTipsButtonText() {
        if (tipsLeft > 0) {
            viewHandler.getTipsButton().setText(String.format("%s (%d left)", activity.getString(R.string.tipTextButton), tipsLeft));
        } else {
            viewHandler.getTipsButton().setText(activity.getString(R.string.tipTextButton));
        }
    }

    public void resetTips() {
        tipsLeft = activity.getInt(R.integer.maxTips);
        setTipsButtonText();
    }

    public void showTip() {
        if (tipsLeft <= 0) {
            showAlert("No more tips for you honey.");
            return;
        }

        tipsLeft--;
        setTipsButtonText();

        int randomIndex = Utils.random(0, stages.get(stageIndex).getDigits() - 1);
        showAlert("Here is your tip",
                String.format("Some digit is %c", String.valueOf(hiddenNumber).charAt(randomIndex)));
    }


    public void resetCurrentGame() {
        Command.resetCurrentGame(this);
    }

    public void initViewEvents() {

        viewHandler.getTipsButton().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                activity.click();
                showTip();
                return true;
            }
        });

        viewHandler.getTipsButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showMessage("Long click to get a tip.");
            }
        });


        viewHandler.getDigitsText().setOnEditorActionListener(new TextView.OnEditorActionListener() {
            @Override
            public boolean onEditorAction(TextView v, int actionId, KeyEvent event) {
                checkGuessedNumber();
                return false;
            }
        });

        viewHandler.getDigitsButton().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showMessage("What are you doing?");
                return true;
            }
        });

        viewHandler.getResetButton().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                activity.click();
                resetCurrentGame();
                return true;
            }
        });

        viewHandler.getResetButton().setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                activity.click();
                showMessage("Click and press it to reset stage.");
            }
        });

        viewHandler.getDigitsText().addTextChangedListener(new numberTypedWatcher(this));

        viewHandler.getGoScoresButton().setOnLongClickListener(new View.OnLongClickListener() {
            @Override
            public boolean onLongClick(View v) {
                showMessage("I'm not ok with your behaviour!");
                return true;
            }
        });
    }

    private void initGame() {

        stages = Stage.loadStages(activity.getContext());
        State state = State.newSingleton(activity);

        if (state.isEmpty()) {
            stageIndex = 0;
            nextStage(true);
            return;
        }

        resetTimer(state.getInt(R.string.remainingTime));
        stageIndex = state.getInt(R.string.stage);
        viewHandler.getClueText().setText(state.getString(R.string.clue));

        viewHandler.getStageText().setText(String.format("Stage %d", stageIndex));

        viewHandler.getDigitsText().setHint(Stage.getHint(stageIndex));
        viewHandler.getDigitsText().setText(state.getString(R.string.number));

        countGuessing = state.getInt(R.string.count);
        hiddenNumber = state.getInt(R.string.hnumber);

        tipsLeft = state.getInt(R.string.tipsLeft);
        setTipsButtonText();

        tips.add(viewHandler.getClueText().getText().toString());
    }

    public void resetTimer() {
        resetTimer(Stage.getMaxTime(stageIndex));
    }

    private void resetTimer(long maxTime) {
        timer.cancel();
        setTimerText(maxTime);
        timer.start(maxTime);
    }

    private void restartGame(boolean showGreetings) {
        resetTimer();
        String message = Utils.greetings(activity.getContext(), stageIndex);
        if (showGreetings) {
            showAlert(message.toString());
        }

        viewHandler.getClueText().setText(message);
        tips.clear();

        viewHandler.getStageText().setText(String.format("Stage %d", stageIndex));
        viewHandler.getDigitsText().setHint(Stage.getHint(stageIndex));

        countGuessing = 0;
        hiddenNumber = Utils.randomize(Stage.getDigits(stageIndex));

        saveState();
    }

    private String readAndCleanDigitsText() {
        String text = viewHandler.getDigitsText().getText().toString();
        viewHandler.getDigitsText().setText("");
        return text;
    }

    public void checkInputChange() {
        if (getDigits() == viewHandler.getDigitsText().length() && !Command.isCommand(viewHandler.getDigitsText().getText())) {
            checkGuessedNumber();
        }
    }

    public void checkGuessedNumber() {
        activity.closeKeyboard();

        ++countGuessing;

        String numberString = readAndCleanDigitsText();

        if (Command.checkCommand(numberString, this)) {
            return;
        }

        if (numberString.length() != Stage.getDigits(stageIndex)) {
            showMessage(Stage.getHint(stageIndex));
            activity.play(R.raw.tryagain);
            return;
        }

        checkHits(numberString);
    }

    private void checkHits(String numberString) {
        int countHits = Utils.countHits(numberString, String.valueOf(hiddenNumber));
        if (countHits == Stage.getDigits(stageIndex)) {
            youWin(numberString);
        } else {
            activity.play(R.raw.tryagain);
            appendClue(numberString, countHits);
        }
    }

    @Override
    public void startVictoryActivity() {
        activity.callNextActivity(VictoryActivity.class);
    }

    public void youWin(String numberString) {
        saveStages(true);

        if (stageIndex == Stage.maxStageIndex()) {
            nextStage(false);
            startVictoryActivity();
            return;
        }

        showAlert("Congrats!", String.format("You have guessed the number %s with %d tries.", numberString, countGuessing));
        nextStage(true);

        activity.click();
        activity.play(R.raw.youwin);
    }

    private void saveStages(boolean win) {
        Stage s = stages.get(stageIndex);
        s.setHits(countGuessing);
        s.setTime(Math.max(0, s.getMaxTime() - timer.getRemainingTime()));
        s.setCleared(win);

        Stage.saveStages(activity.getContext());
    }

    private void nextStage(boolean showGreetings) {
        stageIndex = stageIndex + 1 >= stages.size() ? 1 : stageIndex + 1;
        restartGame(showGreetings);
    }

    private void appendClue(String numberString, int countHits) {
        String noHit = String.format("You typed %s and got no hits.", numberString);
        String someHit = String.format("You typed %s and got %d hits.", numberString, countHits);

        String message = countHits == 0 ? noHit : someHit;

        if (Stage.isShowClues(stageIndex)) {
            tips.add(message);
            message = tips.toString();
        }

        viewHandler.getClueText().setText(message);
    }


    public void showAlert(String message) {
        showAlert(activity.getString(R.string.app_name), message);
    }

    private void showAlert(String title, String message) {
        alert.show(title, message);
    }

    public void showMessage(String message) {
        Toast.makeText(activity.getContext(), message, Toast.LENGTH_LONG).show();
    }

    public State saveState() {

        State state = State.newSingleton(activity);
        state.save(viewHandler.getClueText().getText().toString(), viewHandler.getDigitsText().getText().toString(), hiddenNumber, stageIndex, countGuessing,
                activity.getCurrentAudioPosition(R.raw.back), (int) timer.getRemainingTime(), tipsLeft);
        return state;
    }

    public State saveState(Bundle outState) {
        State state = saveState();
        state.putAllInto(outState);
        return state;
    }

    public void loadStage(Bundle savedInstanceState) {
        viewHandler.getClueText().setText(savedInstanceState.getString("clue"));
        setHiddenNumber(savedInstanceState.getInt("hnumber"));
        setStageIndex(savedInstanceState.getInt("stage"));
        setCountGuessing(savedInstanceState.getInt("count"));
        setTipsLeft(savedInstanceState.getInt("tipsLeft"));

        viewHandler.getDigitsText().setText(savedInstanceState.getString("number"));
    }

    @Override
    public void timeOverCallback(Long remainingTime, String... message) {

        if (remainingTime > 0) {
            setTimerText(remainingTime);
            return;
        }

        try {
            activity.play(R.raw.siren);
        } catch (Exception e) {
            e.printStackTrace();
        }

        if (message != null && message.length > 0) {
            showAlert(message[0]);
        }

        restartGame(true);
    }

    private void setTimerText(Long remainingTime) {
        if (remainingTime <= Timer.ONE_MINUTE) {
            viewHandler.getTimerText().setTextColor(Color.RED);
        } else {
            viewHandler.getTimerText().setTextColor(Utils.NEUTRAL_COLOR);
        }

        viewHandler.getTimerText().setText(Utils.formatTime(remainingTime));
    }

    public void resetStage(int stageIndex) {
        if (!Stage.loadStages(activity.getContext()).containsKey(stageIndex)) {
            return;
        }

        this.stageIndex = stageIndex;
        restartGame(false);
    }

    @Override
    public ActivityPlayable getActivity() {
        return this.activity;
    }

    @Override
    public void setMaxTimer(int timeInMinutes) {
        if(timeInMinutes >= 50){
            timeInMinutes = 50;
        }
        Stage.getStage(stageIndex).setMaxTime(timeInMinutes * 60 * 1000l);
        resetTimer();
    }

    public void setHiddenNumber(int hiddenNumber) {
        this.hiddenNumber = hiddenNumber;
    }

    public void setStageIndex(int stageIndex) {
        this.stageIndex = stageIndex;
    }

    public void setCountGuessing(int countGuessing) {
        this.countGuessing = countGuessing;
    }

    public Map<Integer, Stage> getStages() {
        return stages;
    }

    public void setTipsLeft(int tipsLeft) {
        this.tipsLeft = tipsLeft;
    }

    public int getDigits() {
        return getStages().get(stageIndex).getDigits();
    }

    private void checkInputWhenAnyDigitIsType() {
        activity.setCurrentAudioPosition(R.raw.click, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                activity.click();
                checkInputChange();
            }
        }, 10);
    }

    @Override
    public void numberTypedCallback() {
        checkInputWhenAnyDigitIsType();
    }
}