package com.icsr.game.android.guessthenumber.support;

import android.os.Handler;

import com.icsr.game.android.guessthenumber.support.interfaces.TimeOverable;

import java.util.Date;
import java.util.TimerTask;

public class Timer {
    public static final long MIN_TIME = 30000L;
    public static final Long ONE_MINUTE = 60 * 1000L;
    private final TimeOverable callback;
    private long startTime;
    private long durationInMilsecs;
    private static Handler handler;

    public Timer (TimeOverable clazz) {
        this.callback = clazz;
        handler = new Handler();
    }

    public void start(final long durationInMilsecs) {
        this.startTime = new Date().getTime();

        this.cancel();

        TimerTask timerTask = new TimerTask() {
            @Override
            public void run() {
                runTimer(startTime, durationInMilsecs);
            }
        };

        this.schedule(startTime, 100, durationInMilsecs);

    }

    public void cancel() {
        if(handler != null) {
            handler.removeCallbacksAndMessages(null);
        }
    }

    private void schedule(final long startTime, final int delay, final long durationInMilsecs) {
        handler.postDelayed(new Runnable() {
            @Override
            public void run() {
                runTimer(startTime, durationInMilsecs);

                long elapsedTime = new Date().getTime() - startTime;
                if(elapsedTime < durationInMilsecs){
                    schedule(startTime, delay, durationInMilsecs);
                }
            }
        }, delay);
    }


    private void runTimer(final long startTime, final long durationInMilsecs) {
        this.durationInMilsecs = durationInMilsecs;

        long remainingTime = getRemainingTime();
        callback.timeOverCallback(remainingTime, "Time is over!");
    }

    public long getRemainingTime() {
        long elapsedTime = new Date().getTime() - startTime;
        return durationInMilsecs - elapsedTime;
    }

}
