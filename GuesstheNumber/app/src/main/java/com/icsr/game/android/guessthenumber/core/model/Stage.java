package com.icsr.game.android.guessthenumber.core.model;

import android.content.Context;

import com.icsr.game.android.guessthenumber.support.Utils;

import java.io.Serializable;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;

public class Stage implements Serializable {

    private static final long serialVersionUID = 555122892189L;
    public static final int MAX_TIME_LOWERBOUND = 60 * 1000;

    private int number;
    private boolean showHint;
    private int digits;
    private int hits;

    private long time;
    private long maxTime;
    private static Map<Integer,Stage> allStages = new HashMap<>();
    private static int stageIndex = 0;

    private boolean cleared = false;

    static{
        populateList();
    }

    public static long getMaxTime(int stageIndex) {
        return getStage(stageIndex).getMaxTime();
    }

    public static void resetStages(Context context) {
        allStages.clear();
        populateList();
        Utils.saveStages(context, allStages);
    }

    public long getTime() {
        return time;
    }

    public void setTime(long time) {
        this.time = time;
    }

    public int getHits() {
        return hits;
    }

    public void setHits(int hits) {
        this.hits = hits;
    }

    public long getMaxTime() {
        return maxTime;
    }

    public void setMaxTime(long maxTime) {
        this.maxTime = maxTime;
    }

    private Stage(int number, int digits, long maxTime, boolean showHint) {
        this.number = number;
        this.maxTime = maxTime;
        this.showHint = showHint;
        this.digits = digits;
    }

    private static void populateList() {
        if(!allStages.isEmpty()){
            return;
        }

        allStages.put(1, new Stage(1,2,2*60*1000, true));
        allStages.put(2, new Stage(2,2,2*60*1000, false));
        allStages.put(3,new Stage(3,3, 3*60*1000, true));
        allStages.put(4, new Stage(4,3, 3*60*1000, false));
        allStages.put(5, new Stage(5,4, 5*60*1000, true));
        allStages.put(6, new Stage(6,4, 6*60*1000, false));
        allStages.put(7, new Stage(7,5, 6*60*1000, true));
        allStages.put(8, new Stage(8,5, 8*60*1000, true));
        allStages.put(9, new Stage(9,5, 8*60*1000, true));
        allStages.put(10, new Stage(10,8, 20*60*1000, true));
    }

    public static String getHint(int stageIndex) {
        return getStage(stageIndex).getHint();
    }

    public static int getDigits(int stageIndex) {
        return getStage(stageIndex).getDigits();
    }

    public static boolean isShowClues(int stageIndex) {
        return getStage(stageIndex).isShowClues();
    }

    public static long getTime(int stageIndex){
        return getStage(stageIndex).getMaxTime();
    }

    public int getDigits() {
        return digits;
    }

    public int getNumber() {
        return number;
    }

    public boolean isShowClues() {
        return showHint;
    }

    public String getHint(){
        return String.format("Type a %d-digit number", getDigits());
    }

    public static boolean saveStages(Context context){
        return Utils.saveStages(context, allStages);
    }

    public static Map<Integer, Stage> loadStages(Context context) {
        Map<Integer, Stage> stgs = Utils.loadStages(context);

        if(!stgs.isEmpty()){

            for(Integer k : stgs.keySet()){
                stgs.get(k).setMaxTime(allStages.get(k).getMaxTime());
            }

            allStages.clear();
            allStages.putAll(stgs);
        }
        return allStages;
    }
    public static Stage getStage(int stageIndex){
        stageIndex = Math.max(1, stageIndex);
        stageIndex = Math.min(allStages.size(), stageIndex);
        return allStages.get(stageIndex);
    }

    public static int maxStageIndex(){               
        return  Collections.max(allStages.keySet());
    }

    public boolean isCleared() {
        return cleared;
    }

    public void setCleared(boolean cleared) {
        this.cleared = cleared;
    }
}
