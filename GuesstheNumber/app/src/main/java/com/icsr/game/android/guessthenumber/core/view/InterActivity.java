package com.icsr.game.android.guessthenumber.core.view;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Vibrator;
import android.view.inputmethod.InputMethodManager;

import androidx.annotation.Nullable;
import androidx.appcompat.app.AppCompatActivity;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.business.AudioHolder;
import com.icsr.game.android.guessthenumber.support.FileUtils;
import com.icsr.game.android.guessthenumber.core.model.Stage;
import com.icsr.game.android.guessthenumber.core.model.State;
import com.icsr.game.android.guessthenumber.support.Utils;
import com.icsr.game.android.guessthenumber.support.interfaces.ActivityPlayable;

import java.util.List;

public abstract class InterActivity extends AppCompatActivity implements ActivityPlayable {

    protected Vibrator vibrator;
    protected AudioHolder audio;
    protected FileUtils<List<Stage>> fileUtils;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);

        initMain();
        setContentView(getLayoutRid());
        init();
    }

    private void initMain() {
        initFileUtils();
        initVibrator();
        initSound();
    }

    private void initFileUtils() {
        fileUtils = new FileUtils<>(getApplicationContext());
    }

    private void initVibrator() {
        vibrator = (Vibrator) getSystemService(VIBRATOR_SERVICE);
    }

    private void initSound() {
        this.audio = new AudioHolder(this);
    }

    @Override
    public void closeKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus().getWindowToken(), InputMethodManager.RESULT_UNCHANGED_SHOWN);

        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void callNextActivity(final Class<? extends Activity> activity, int delay) {
        audio.pauseAll();
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                startActivity(new Intent(getApplicationContext(), activity));
                finish();
            }
        }, delay);
    }

    @Override
    public void callNextActivity(final Class<? extends Activity> activity) {
        callNextActivity(activity, 1);
    }

    @Override
    public Context getContext() {
        return this.getApplicationContext();
    }

    @Override
    public int getInt(int R_ID_KEY) {
        return getResources().getInteger(R_ID_KEY);
    }

    @Override
    public float getFloat(int R_ID_KEY){
        String n = getString(R_ID_KEY);
        if(!Utils.isNumeric(n)){
            return 0f;
        }
        return Float.parseFloat(n);
    }

    @Override
    public SharedPreferences getPreferences() {
        return getSharedPreferences(State.class.getName(), Context.MODE_PRIVATE);
    }

    @Override
    public void rumble(int R_ID) {
        if (!vibrator.hasVibrator()) {
            return;
        }

        vibrator.vibrate(getInt(R_ID));
    }

    @Override
    public void click() {
        rumble(R.integer.clickRumbleDuration);
        audio.setCurrentPosition(R.raw.click, 0);
        new Handler().postDelayed(new Runnable() {
            @Override
            public void run() {
                audio.click();
            }
        }, 10);
    }

    @Override
    public void play(int R_ID) {
        audio.play(R_ID);
    }

    @Override
    public int getCurrentAudioPosition(int R_ID) {
        return audio.getCurrentPosition(R_ID);
    }

    @Override
    public void setCurrentAudioPosition(int R_ID, int position) {
        audio.setCurrentPosition(R_ID, position);
    }
}
