package com.icsr.game.android.guessthenumber.support.interfaces;

public interface TimeOverable<T> {
    void timeOverCallback(T variable, String... message);
}
