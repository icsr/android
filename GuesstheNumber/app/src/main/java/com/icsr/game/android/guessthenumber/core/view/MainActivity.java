package com.icsr.game.android.guessthenumber.core.view;

import android.os.Bundle;
import android.view.View;
import android.view.inputmethod.EditorInfo;
import android.widget.Button;
import android.widget.TextView;

import androidx.annotation.NonNull;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.business.GameHandler;
import com.icsr.game.android.guessthenumber.core.business.ViewHolder;

public class MainActivity extends InterActivity {

    private Button digitsButton;
    private Button resetButton;
    private TextView digitsText;
    private View root;
    private TextView stageText;
    private TextView timerText;

    private Button goScoresButton;
    private Button tipsButton;
    private TextView clueText;
    private GameHandler gameHandler;

    @Override
    public int getLayoutRid() {
        return R.layout.activity_main;
    }

    @Override
    public void init() {
        initSound();
        initView();
        initGame();
    }

    public void initSound() {
        audio.play(R.raw.back, 1000);
    }

    private void initEvents() {
        goScoresButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callNextActivity(ScoresActivity.class);
            }
        });

        digitsButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                click();
                gameHandler.checkGuessedNumber();
                closeKeyboard();
            }
        });
    }

    private void initGame() {
        gameHandler = new GameHandler(this, new ViewHolder(tipsButton, digitsButton, goScoresButton,
                resetButton, clueText, digitsText, stageText, timerText));
        initEvents();
    }

    public void initView() {
        setContentView(R.layout.activity_main);
        timerText = findViewById(R.id.timerText);
        digitsText = findViewById(R.id.digitsText);
        clueText = findViewById(R.id.clueText);
        root = findViewById(R.id.root);
        stageText = findViewById(R.id.stageView);
        tipsButton = findViewById(R.id.tipButton);
        digitsButton = findViewById(R.id.digitsButton);
        goScoresButton = findViewById(R.id.goScoresButton);
        resetButton = findViewById(R.id.resetButton);
        digitsText.setImeOptions(EditorInfo.IME_ACTION_DONE);
    }

    @Override
    protected void onSaveInstanceState(@NonNull Bundle outState) {
        gameHandler.saveState(outState);
        super.onSaveInstanceState(outState);
    }

    @Override
    protected void onRestoreInstanceState(@NonNull Bundle savedInstanceState) {
        super.onRestoreInstanceState(savedInstanceState);

        if (savedInstanceState.isEmpty() || !savedInstanceState.containsKey("clue")) {
            return;
        }

        gameHandler.loadStage(savedInstanceState);

    }

}