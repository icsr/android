package com.icsr.game.android.guessthenumber.core.view;

import android.view.View;
import android.widget.Button;
import android.widget.TextView;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.business.AudioHolder;
import com.icsr.game.android.guessthenumber.core.business.Score;
import com.icsr.game.android.guessthenumber.core.model.Stage;
import com.icsr.game.android.guessthenumber.support.Utils;

import java.util.Arrays;
import java.util.Collections;
import java.util.List;
import java.util.Map;

public class ScoresActivity extends InterActivity {

    private TextView scoresText;
    private Button okButton;

    @Override
    public void init() {
        initSound();
        initView();
        initViewEvents();
        showScores();
    }

    @Override
    public int getLayoutRid() {
        return R.layout.activity_scores;
    }

    private void showScores() {
        Map<Integer, Stage> stages = Stage.loadStages(this);

        scoresText.setText("These are your current SCORES:");

        List<Integer> keys = Arrays.asList(stages.keySet().toArray(new Integer[0]));
        Collections.sort(keys);

        for (Integer k : keys) {
            Stage s = stages.get(k);
            scoresText.append("\n");
            if (s.getHits() <= 0) {
                scoresText.append(String.format("Stage %2d - not cleared", s.getNumber()));
            } else {
                scoresText.append(String.format("Stage %2d - %3d hits - %s s - %d pts",
                        s.getNumber(), s.getHits(), Utils.formatTime(s.getTime()), Score.getScore(this, s)));
            }
        }

        scoresText.append(String.format("\n\nTOTAL: %s", Utils.formatPoints(Score.getScores(this))));
    }

    private void initViewEvents() {

        okButton.setText(getString(R.string.startButtonText));

        okButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                audio.click();
                callNextActivity(MainActivity.class);
            }
        });
    }

    private void initSound() {
        audio = new AudioHolder(this);
        audio.play(R.raw.back0, 1000);
    }

    private void initView() {
        scoresText = findViewById(R.id.scoreText);
        okButton = findViewById(R.id.okScoresButton);
    }
}
