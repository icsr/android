package com.icsr.game.android.guessthenumber.support;

import androidx.annotation.NonNull;

import java.util.LinkedList;

public class Tips extends LinkedList<String> {

    @NonNull
    @Override
    public String toString() {
        StringBuilder str = new StringBuilder();

        for(String s : this){
            str.append(s);
            str.append("\n");
        }

        return str.toString();
    }

    @Override
    public boolean add(String s) {
        super.addFirst(s);
        return true;
    }
}
