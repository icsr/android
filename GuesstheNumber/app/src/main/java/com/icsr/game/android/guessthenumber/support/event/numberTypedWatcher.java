package com.icsr.game.android.guessthenumber.support.event;

import android.text.Editable;
import android.text.TextWatcher;

import com.icsr.game.android.guessthenumber.support.interfaces.NumberTypedCallable;

public class numberTypedWatcher implements TextWatcher {

    private final NumberTypedCallable callback;

    public numberTypedWatcher(NumberTypedCallable callback) {
        this.callback = callback;
    }


    @Override
    public void beforeTextChanged(CharSequence s, int start, int count, int after) {

    }

    @Override
    public void onTextChanged(CharSequence s, int start, int before, int count) {

    }

    @Override
    public void afterTextChanged(Editable s) {
        callback.numberTypedCallback();
    }

}
