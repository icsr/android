package com.icsr.game.android.guessthenumber.support.interfaces;

import android.content.Context;

public interface Playable extends NumberTypedCallable, TimeOverable<Long> {

    void startVictoryActivity();
    void youWin(String numberString);

    void resetStage(int stageIndex);

    void setMaxTimer(int time);

    void resetTips();

    void showAlert(String s);

    ActivityPlayable getActivity();

    void resetTimer();
}
