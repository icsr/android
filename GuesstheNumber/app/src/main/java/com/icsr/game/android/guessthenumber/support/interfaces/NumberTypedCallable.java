package com.icsr.game.android.guessthenumber.support.interfaces;

public interface NumberTypedCallable {
    void numberTypedCallback();
}
