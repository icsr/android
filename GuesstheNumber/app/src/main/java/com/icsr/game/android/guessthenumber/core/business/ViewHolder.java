package com.icsr.game.android.guessthenumber.core.business;

import android.widget.Button;
import android.widget.TextView;

public class ViewHolder {

    private final TextView clueText;
    private final TextView digitsText;
    private final TextView stageText;
    private final TextView timerText;
    private final Button resetButton;
    private final Button digitsButton;
    private final Button goScoresButton;
    private final Button tipsButton;

    public ViewHolder(Button tipsButton, Button digitsButton, Button goScoresButton, Button resetButton, TextView clueText, TextView digitsText, TextView stageText, TextView timerText) {
        this.clueText = clueText;
        this.digitsText = digitsText;
        this.stageText = stageText;
        this.timerText = timerText;
        this.tipsButton = tipsButton;
        this.digitsButton = digitsButton;
        this.goScoresButton = goScoresButton;
        this.resetButton = resetButton;
    }

    public TextView getClueText() {
        return clueText;
    }

    public TextView getDigitsText() {
        return digitsText;
    }

    public TextView getStageText() {
        return stageText;
    }

    public TextView getTimerText() {
        return timerText;
    }

    public Button getResetButton() {
        return resetButton;
    }

    public Button getDigitsButton() {
        return digitsButton;
    }

    public Button getGoScoresButton() {
        return goScoresButton;
    }

    public Button getTipsButton() {
        return tipsButton;
    }
}
