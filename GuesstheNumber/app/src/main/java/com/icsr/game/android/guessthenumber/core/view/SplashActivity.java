package com.icsr.game.android.guessthenumber.core.view;

import com.icsr.game.android.guessthenumber.R;

public class SplashActivity extends InterActivity {

    @Override
    public int getLayoutRid() {
        return R.layout.activity_splash;
    }

    @Override
    public void init() {
        callNextActivity(ScoresActivity.class, getInt(R.integer.splash_delay));
    }

}
