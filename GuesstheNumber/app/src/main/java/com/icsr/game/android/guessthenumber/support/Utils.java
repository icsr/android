package com.icsr.game.android.guessthenumber.support;

import android.content.Context;
import android.graphics.Color;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.model.Stage;

import java.text.DecimalFormat;
import java.util.HashMap;
import java.util.Map;

public abstract class Utils {

    public static final int NEUTRAL_COLOR = Color.rgb(255, 252, 213);

    public static int randomize(int qtyOfDigits) {
        qtyOfDigits = Math.max(qtyOfDigits, 1);

        while (true) {
            int n = (int) (Math.pow(10, qtyOfDigits) * Math.random());
            String str = String.valueOf(n);
            if (str.length() == qtyOfDigits && str.toCharArray()[0] != 0) {
                return n;
            }
        }
    }

    public static int countHits(String string1, String string2) {
        char[] char1 = string1.toCharArray();
        char[] char2 = string2.toCharArray();

        int countHits = 0;
        for (int i = 0; i < char1.length; i++) {
            if (char1[i] == char2[i]) {
                ++countHits;
            }
        }
        return countHits;
    }

    public static String greetings(Context context, int stageIndex) {
        StringBuilder message = new StringBuilder();
        message.append(stageIndex == 1 ? String.format("Hi there. Welcome to %s.\n\n", context.getString(R.string.app_name)) : "");
        message.append(String.format("You're on Stage %d.\nCan you guess the %d-digit number with %sclue?\n", stageIndex, Stage.getDigits(stageIndex), Stage.isShowClues(stageIndex) ? "" : "no "));

        return message.toString();
    }

    public static boolean isNumeric(String string) {
        try {
            Double.parseDouble(string);
            return true;
        } catch (Exception e) {
            return false;
        }
    }

    public static boolean saveStages(Context context, Map<Integer, Stage> stages) {
        return new FileUtils<>(context).save(R.string.scores_key, stages);
    }

    public static Map<Integer, Stage> loadStages(Context context) {

        FileUtils<Map<Integer, Stage>> fileUtils = new FileUtils<>(context);

        Map<Integer, Stage> stages = new HashMap<>();

        if(!fileUtils.exists(R.string.scores_key)){
            return stages;
        }

        try {
            stages.putAll(fileUtils.load(R.string.scores_key));
        } catch (Exception e) {
            e.printStackTrace();
        }

        return stages;
    }

    public static String formatTime(long remainingTime) {
        int h = (int) (remainingTime / 1000 / 60 / 60);
        int m = (int) (remainingTime / 1000 / 60);
        int s = (int) ((remainingTime / 1000) % 60);

        StringBuilder str = new StringBuilder();
        if (h > 0) {
            str.append(String.format("%2d", h));
            str.append(":");
        }

        str.append(String.format("%02d", m));
        str.append(":");
        str.append(String.format("%02d", s));

        return str.toString();
    }

    public static String formatNumber(int number) {
        DecimalFormat decimalFormat = new DecimalFormat();
        return decimalFormat.format(number);
    }

    public static String formatPoints(int scores) {
        if (scores <= 0) {
            return "you got nothing yet.";
        }

        return formatNumber(scores) + " points";
    }

    public static int random(int start, int end) {
        while (true) {
            int n = (int) (Math.random() * 10);
            if (n >= start && n <= end) {
                return n;
            }
        }
    }

    public static int getInt(Context context, int R_ID_KEY) {
        String str = context.getString(R_ID_KEY);
        if (!isNumeric(str)) {
            return -1;
        }
        return Integer.parseInt(str);
    }
}
