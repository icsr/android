package com.icsr.game.android.guessthenumber.support;

import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.core.model.Stage;
import com.icsr.game.android.guessthenumber.core.model.State;
import com.icsr.game.android.guessthenumber.support.Utils;
import com.icsr.game.android.guessthenumber.support.interfaces.Playable;

import java.util.HashSet;
import java.util.Map;
import java.util.Set;

public class Command {

    public static boolean isCommand(CharSequence code) {
        return code.charAt(0) == '0';
    }

    public enum COMMAND {
        SHOW_ABOUT(true, "0901"), GOTO_STAGE(true, "02xx"),
        SET_TIME(true, "06xx"), YOU_WIN(true, "0777"),
        SHOW_VICTORY_SCREEN(true, "0101"), RESET_ALL_GAMES(false, "0000"),
        CLEAR_ALL_STAGES(false, "0144000777"),
        UNCLEAR_ALL_STAGES(false, "0144000700"), RESET_TIPS(false, "0171"),
        RESET_CURRENT_GAME(false, "0500"), NO_COMMAND(false, "xxxx");
        private final boolean showOnVictory;
        private final String code;

        COMMAND(boolean common, String code) {
            this.showOnVictory = common;
            this.code = code;
        }

        public static COMMAND get(String code) {
            for (COMMAND c : COMMAND.values()) {
                if (c.getCode().equals(code)) {
                    return c;
                }
            }
            return NO_COMMAND;
        }

        public static Set<String> listCommons() {
            Set<String> commands = new HashSet<>();
            for (COMMAND c : COMMAND.values()) {
                if (c.isShowOnVictory()) {
                    commands.add(c.getCode());
                }
            }
            return commands;
        }

        public boolean isShowOnVictory() {
            return showOnVictory;
        }

        public String getCode() {
            return code;
        }
    }

    ;


    public static boolean checkCommand(String numberString, Playable main) {
        return checkCommand(numberString, main, false);
    }

    private static boolean checkCommand(String numberString, Playable main, boolean insideCommand) {

        if (processSuperCommands(numberString, main)) return true;

        if (notCommand(numberString)) {
            return false;
        }

        if (!clearedGame(main) && !insideCommand) {
            main.showAlert(main.getActivity().getString(R.string.somethingIsMissing));
            return false;
        }

        return processCommonCommands(numberString, main);
    }

    private static boolean processCommonCommands(String code, Playable main) {

        if (isGotoStageCommand(code)) {
            processGotoStage(code, main);
            return true;
        }

        if (isSetTimeCommand(code)) {
            processSetTime(code, main);
            return true;
        }

        switch (COMMAND.get(code)) {
            case SHOW_ABOUT:
                main.showAlert(main.getActivity().getString(R.string.about));
                return true;
            case SHOW_VICTORY_SCREEN:
                main.startVictoryActivity();
                return true;
            case RESET_ALL_GAMES:
                State.resetAllGames();
                Stage.resetStages(main.getActivity().getContext());
                main.resetStage(1);
                main.resetTimer();
                return true;
            default:
                return false;
            case RESET_CURRENT_GAME:
                main.timeOverCallback(0l);
                return true;
            case YOU_WIN:
                main.youWin(COMMAND.YOU_WIN.getCode());
                return true;
            case RESET_TIPS:
                main.resetTips();
                return true;
        }
    }

    private static void processSetTime(String numberString, Playable main) {
        try {
            String str = numberString.substring(2);
            main.setMaxTimer(Integer.parseInt(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static void processGotoStage(String numberString, Playable main) {
        try {
            String str = numberString.substring(2);
            main.resetStage(Integer.parseInt(str));
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static boolean isSetTimeCommand(String numberString) {
        return numberString.charAt(1) == COMMAND.SET_TIME.getCode().charAt(1);
    }

    private static boolean isGotoStageCommand(String numberString) {
        return numberString.charAt(1) == COMMAND.GOTO_STAGE.getCode().charAt(1);
    }

    private static boolean notCommand(String numberString) {
        return numberString == null || numberString.length() != 4 || numberString.charAt(0) != '0';
    }

    private static boolean processSuperCommands(String numberString, Playable main) {
        switch (COMMAND.get(numberString)) {
            case CLEAR_ALL_STAGES:
                setclearedAllStages(main, true);
                return true;
            case UNCLEAR_ALL_STAGES:
                setclearedAllStages(main, false);
                return true;
        }
        return false;
    }

    private static void setclearedAllStages(Playable main, boolean cleared) {
        Map<Integer, Stage> stages = Stage.loadStages(main.getActivity().getContext());
        for (Stage s : stages.values()) {
            s.setCleared(cleared);
        }
        Utils.saveStages(main.getActivity().getContext(), stages);
    }

    private static boolean clearedGame(Playable main) {
        boolean clearedGame = true;

        for (Stage s : Stage.loadStages(main.getActivity().getContext()).values()) {
            clearedGame &= s.isCleared();
            if (!clearedGame) {
                break;
            }
        }

        return clearedGame;
    }

    public static void resetCurrentGame(Playable main) {
        checkCommand(COMMAND.RESET_CURRENT_GAME.getCode(), main, true);
    }
}
