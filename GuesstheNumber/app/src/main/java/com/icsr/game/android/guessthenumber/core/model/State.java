package com.icsr.game.android.guessthenumber.core.model;

import android.content.SharedPreferences;
import android.os.Bundle;


import com.icsr.game.android.guessthenumber.R;
import com.icsr.game.android.guessthenumber.support.interfaces.ActivityPlayable;

import java.util.Map;

public class State {

    ActivityPlayable activityPlayable;

    static SharedPreferences preferences;

    private static State state = null;

    public static State newSingleton(ActivityPlayable context){
        if(state == null){
            state = new State(context);
        }else{
            state.activityPlayable = context;
        }

        return state;
    }

    private State(ActivityPlayable context) {
        this.activityPlayable = context;
        this.preferences = context.getPreferences();
    }

    public static void resetAllGames() {
        SharedPreferences.Editor edit = preferences.edit();
        edit.clear();
        edit.apply();
    }

    public boolean isEmpty(){
        return preferences == null || preferences.getAll().isEmpty();
    }

    public boolean hasKey(int key){
        return !isEmpty() && preferences.getAll().containsKey(activityPlayable.getString(key));
    }

    public int getInt(int key){
        return getInt(key, -1);
    }

    public int getInt(int key, int defaultValue){
        return preferences.getInt(activityPlayable.getString(key), defaultValue);
    }

    public String getString(int key){
        return preferences.getString(activityPlayable.getString(key), "");
    }

    private void putString(int key, String value, SharedPreferences.Editor editor) {
        editor.putString(activityPlayable.getString(key), value);
    }

    private void putInt(int key, int value, SharedPreferences.Editor editor) {
        editor.putInt(activityPlayable.getString(key), value);
    }

    public Map<Integer, Stage> getStages(){
        return Stage.loadStages(activityPlayable.getContext());
    }

    public void save(String clueText, String digitsText, int hiddenNumber, int stageIndex, int countGuessing, int musicPosition, int remainingTime, int tipsLeft){
        SharedPreferences.Editor editor = preferences.edit();
        putString(R.string.clue, clueText, editor);
        putString(R.string.number, digitsText, editor);
        putInt(R.string.hnumber, hiddenNumber, editor);
        putInt(R.string.stage, stageIndex, editor);
        putInt(R.string.count, countGuessing, editor);
        putInt(R.string.music, musicPosition, editor);
        putInt(R.string.remainingTime, remainingTime, editor);
        putInt(R.string.tipsLeft, tipsLeft, editor);
        editor.commit();
    }

    public void putAllInto(Bundle bundle){
        for(Map.Entry<String, ?> e : preferences.getAll().entrySet()){
            if(e.getValue() instanceof Integer){
                bundle.putInt(e.getKey(), Integer.class.cast(e.getValue()));
            }else if(e.getValue() instanceof String){
                bundle.putString(e.getKey(), String.class.cast(e.getValue()));
            }
        }
    }

}
